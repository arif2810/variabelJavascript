// Hoisting pada let

// name = 'Bot';
// let name;
// console.log(name);


// Revisi Code
// let name = "Bot";
// console.log(name);

// Hoisting pada Function
let message = "Hello";
function greeeting(){
  console.log(message);
  let message = "Hello World";
}
greeeting();

//Revisi Code
// let message = "Hello";
// function greeeting(){
//   let message = "Hello World";
//   console.log(message);
// }
// greeeting();


